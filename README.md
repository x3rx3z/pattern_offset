```
./pattern_offset.py 64
A0A1A2A3A4A5A6A7A8A9B0B1B2B3B4B5B6B7B8B9C0C1C2C3C4C5C6C7C8C9D0D1
```

```
./pattern_offset.py 0x35413441
0x35413441 -> A4A5
Memory offset to exploit: 8 bytes.

Test with:
python -c 'print "A"*8 + "BBBB"'
```

